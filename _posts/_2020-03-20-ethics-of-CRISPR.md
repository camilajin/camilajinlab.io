---
layout: post
title:  "Ethics of CRISPR"
date:   2020-03-20 15:32:14 -0300
categories: bioengineering
---
# The Ethics of CRISPR

>"My real resumé was in my cells." How many times have we wished to be different? "I wish I am taller.""I wish I am better at math.""I wish I don't have HIV." Now imagine living in a world where all our "imperfections" can be changed. That is exactly the way that the world in [Gattaca](https://www.imdb.com/title/tt0119177/reviews) works, and perhaps, the way our not-distant future will work.

### Background 

Throughout the history of humanity, biotechnology has been used to fight against hunger and malnutrition. In ancient times, soon after civilization, we used lactic fermentation and alcoholic fermentation to keep the food fresh. In 1973, there was a turning point for biotechnology. With the discovery of the recombinant DNA technique in which scientists used it to manipulate the genetic combinations of genes, genetic engineering was born.

In the 1980s, genetic engineering was not really thought to be controversial. Through genetic engineering techniques such as cell selection and cloning, scientists managed to grow healthier crops. In other words, this type of engineering had always been beneficial to humanity. In the last few years, however, ethical questions regarding genetic engineering have begun to appear. The newest and most popular genetic engineering tool now- CRISPR-Cas9 can edit sections of the genome by targeting, removing and changing parts of the DNA sequence. It sounds very easy to manage, right? At first sight, CRISPR seems to be perfect- the genetic editing tool that scientists had been searching for centuries. However, CRISPR is not perfect and it may interfere with normal genetic operation. The use of CRISPR in humans would be an unprofessional and unethical decision from scientists and bioengineers.

Janet Rossant is a developmental biologist well known for her contributions to the understanding of the role of genes in embryo development. Writing in Development, the leading primary research journal in the field of developmental biology, she explains the three different ways CRISPR-CAS9 gene editing can be used in humans. First, as a basic research tool in human cells or embryos to help understand normal development, copy human disease, and develop new treatments for that disease. Second, for gene editing in living cells, either in a lab directly in the organism to treat or prevent disease. Third, for gene editing in sperm, female human eggs, or embryos with the idea of correcting mutations that might cause disease in offspring. Most of the ethical discussions take place around this third type of gene editing called germline gene editing in humans.

### World's First Gene Edited Babies

In November 2018, Chinese biophysics researcher Jiankui He carried out the first genome-editing experiment in prenatal twins to successfully resist HIV infection. Although having children who can successfully resist HIV infection is extremely beneficial, is it ethical? Also does editing the girls' DNA then leave them open to other types of infections? He used CRISPR technology to edit the girls' genes while they were embryos. When the girls were born, they became the first humans to have had their genes edited in vitro. Chinese investigators confirmed his claim.

Although his use of germline engineering seems to be advantageous for the twins, it raises ethical questions. What if the mutations he introduced in the girls' DNA could pass on genetic diseases to their children? Also, is it ethical to edit the genes of embryos who cannot give consent to participate in this type of experiment? “Every fetus has the right to remain genetically unmodified.” We do not have the right to use CRISPR editing on them. “Offspring do not consent to their parents’ intentional exposure to mutagenic sources that alter the germline,” said George Church, a geneticist at Harvard Medical School. Before performing procedures on any human being, we shall have the consent of the individual. When it comes to children, parents' consent is not enough since the risks would only affect the individual.

According to the Center for Genetics and Society, “ using the same techniques to modify embryos in order to make permanent, irreversible changes to future generations and to our common genetic heritage is far more problematic.” Once the genetic makeup is changed, the offspring's genetic would also be changed. Thus, if CRISPR-Cas9 has any secondary effect, all the future generations will inherit the defect. Indeed, according to the  International Summit on Human Gene Editing, "until germline genome editing is deemed safe through research, it should not be used for clinical reproductive purposes." Therefore, further research is needed to find out the possible side effects. "The risk cannot be justified by the potential benefit" added the ISHGE.

Speaking about the twin girls in China, “The answer is likely yes, it did affect their brains,” remarks Alcino J. Silva, a neurobiologist at the University of California Los Angeles. Eventually, researchers have already found out that CRISPR editing had some impact on their brains. “The simplest interpretation is that those mutations will probably have an impact on cognitive function in the twins but the exact effect on the girls' cognition is impossible to predict,” added Silva. Therefore, he concluded, “that is why it should not be done.”

Genetic Engineering is developing at a very fast rate but there are still many factors not yet discovered within this broad field. CRISPR makes the dream of creating a genetically perfect family closer to reality, however, further research is needed in order to fully understand it and use it on humans. Some scientists are always trying to be ahead, violating ethics. If you want to learn more about CRISPR technology, there are a few articles that I found that may interest you: "Introducing the World’s First Gene-Edited Lizard" and "CRISPR crisis: China wants to protect its genetic engineering reputation."

The future is now. Advances in bio-engineering and molecular biology have taken us closer to the thin a blurry line of distinction between ethical and unethical. Is science leading our species to destruction or to a future of genetically perfect humans? Only time will tell.

### Reference 


- Ayres, Crystal. “5 Standout Pros and Cons of Human Genetic Engineering.” Green Garage, 15 July 2015, greengarageblog.org/5-standout-pros-and-cons-of-human-genetic-engineering. 
- Ayres, Crystal. “13 Advantages and Disadvantages of Genetic Engineering.” Vittana.org, vittana.org/13-advantages-and-disadvantages-of-genetic-engineering. 
- In Genetics, ST. “Benefits and Ethical Concerns of CRISPR - Pros and Cons.” Explore Biotech, 22 Feb. 2019, explorebiotech.com/crispr-pros-and-cons/. 
- Murphy, Heather. “Introducing the World's First Gene-Edited Lizard.” The New York Times, The New York Times, 5 Apr. 2019, www.nytimes.com/2019/04/05/us/crispr-albino-lizard.html. https://www.sciencemag.org/news/2019/02/china-tightens-its-regulation-some-human-gene-editing-labeling-it-high-risk
- Normile, Dennis. “China Tightens Its Regulation of Some Human Gene Editing, Labeling It 'High-Risk'.” Science, 28 Feb. 2019, www.sciencemag.org/news/2019/02/china-tightens-its-regulation-some-human-gene-editing-labeling-it-high-risk.
- Rathi, Akshat, and Akshat Rathi. “The Pros and Cons of Genetically Engineering Your Children.” Quartz, Quartz, 26 Nov. 2018, qz.com/564649/the-pros-and-cons-of-genetically-engineering-your-children/. 
- Harris, John, and Marcy Darnovsky. “Pro and Con: Should Gene Editing Be Performed on Human Embryos?” National Geographic, 26 Nov. 2018, www.nationalgeographic.com/magazine/2016/08/human-gene-editing-pro-con-opinions/. 


Written in 10/IV/2019, edited in 21/III/2020
