#!/bin/bash

TITLE=untitled
TITLE_SET=false
HIDDEN_POST=false
DAY=unset
DAY_SET=false

usage() {
	echo "Usage: $0 -t <title> [-H -d YYYY-MM-DD]" 1>&2;
	echo "	t: title name, mandatory."
	echo "	d: day. Optional. If not provided, the system's current day will be used."
	echo "	H: hide file from blog with '_'."
}

exit_usage() {
	usage
	exit 1;
}

while getopts "d:t:h:H" o; do
    case "${o}" in
        d)
			DAY=${OPTARG}
			# TODO check input valid or exit
			DAY_SET=true
            ;;
        t)
            TITLE_ARG=${OPTARG}
			TITLE_LEN=${#TITLE_ARG}
            (($TITLE_LEN != 0)) || exit_usage
			# TODO check TITLE valid
			TITLE=$TITLE_ARG
			TITLE_SET=true
            ;;
        H)
			HIDDEN_POST=true
            ;;
        h)
           	exit_usage
            ;;
        *)
           	exit_usage
            ;;
    esac
done

# get options values
if [[ $TITLE_SET = false ]]; then
	echo "Please set title."
	usage
	exit 1;
fi
if [[ $DAY_SET = false ]]; then
	DAY=$(date +%Y-%m-%d)
fi

# create file
FILE="${DAY}-${TITLE}.md"
if [[ $HIDDEN_POST = true ]]; then
	FILE="_$FILE"
fi
if [[ -f "$FILE" ]]; then
	echo "${FILE} exists. Please pick a different title."
	exit 1;
fi
touch $FILE
# add header to file
echo "---" >> $FILE
echo "layout: post" >> $FILE
echo "title: $TITLE" >> $FILE
echo "categories: uncatagorized" >> $FILE
echo "---" >> $FILE

echo "${FILE} has been created."
